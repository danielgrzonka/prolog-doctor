﻿%% :- discontiguous trwa/2.
%% :- discontiguous powoduje/2.
:- style_check(-singleton).

%% ---------------------------------------------------------------------------------
%% ---------------------------------------------------------------------------------
%% ---------------------------------------------------------------------------------
%% *********************************** KNOWLEDGE ***********************************

/*
***********************************************
******          PRZEZIEBIENIE           *******
***********************************************
*/
powoduje(choroba(przeziebienie), kichanie).
powoduje(choroba(przeziebienie), katar).
powoduje(choroba(przeziebienie), oslabienie).


/*
***********************************************
******            ALERGIA              *******
***********************************************
*/
powoduje(choroba(alergia), kichanie).
powoduje(choroba(alergia), katar).
powoduje(choroba(alergia), opuchlizna).
powoduje(choroba(alergia), dusznosc).


/*
***********************************************
******              GRYPA               *******
***********************************************
*/
powoduje(choroba(grypa), goraczka).
powoduje(choroba(grypa), kaszel).
powoduje(choroba(grypa), katar).
powoduje(choroba(grypa), bol).
powoduje(choroba(grypa), oslabienie).
powoduje(choroba(grypa), dreszcze).
powoduje(choroba(grypa), brak_apetytu).




/*
***********************************************
******             MIGRENA              *******
***********************************************
*/
powoduje(choroba(migrena), bol).
powoduje(choroba(migrena), swiatlowstret).
powoduje(choroba(migrena), fonofobia).%%nadwrazliwosc na dzwieki
powoduje(choroba(migrena), nudnosci).
powoduje(choroba(migrena), wymioty).



/*
***********************************************
******         FOBIA SPOLECZNA          *******
***********************************************
*/
powoduje(choroba("fobia spoleczna"), lek).
powoduje(choroba("fobia spoleczna"), drzenie_rak).
powoduje(choroba("fobia spoleczna"), dusznosc).
powoduje(choroba("fobia spoleczna"), niesmialosc).
powoduje(choroba("fobia spoleczna"), rumieniec).
powoduje(choroba("fobia spoleczna"), nadpotliwosc).

/*
***********************************************
******         ZAWAL SERCA          *******
***********************************************
*/

powoduje(choroba("zawał serca"), bol).
powoduje(choroba("zawał serca"), panika).
powoduje(choroba("zawał serca"), lek).
powoduje(choroba("zawał serca"), dusznosc).
powoduje(choroba("zawał serca"), bladosc).
powoduje(choroba("zawał serca"), spadek_cisnienia).
powoduje(choroba("zawał serca"), pobudzenie_ruchowe).
powoduje(choroba("zawał serca"), oslabienie).
powoduje(choroba("zawał serca"), goraczka).

/*
***********************************************
********         ZAWAL SERCA          *********
***********************************************
*/

powoduje(choroba("parkinson"), drzenie_rak).
powoduje(choroba("parkinson"), niezgrabnosc).
powoduje(choroba("parkinson"), zaburzenia_rownowagi).
powoduje(choroba("parkinson"), spowolnienie_ruchowe).
powoduje(choroba("parkinson"), zaburzenia_pamieci).

